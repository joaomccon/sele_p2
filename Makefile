.DEFAULT_GOAL := instrucoes

instrucoes:
	@echo "COMANDOS COMPILACAO"
	@echo "	Para compilar: 					make compile"
	@echo "	Para limpar directoria:  			make clean"
	@echo ""
	@echo "COMANDOS SIMULACOES (sempre de 1 a 5 servidores)"
	@echo "	Para simular sem fila: 				make simulation_no_q"
	@echo "	Para simular com fila infinita:  		make clean"
	@echo ""
	@echo "COMANDOS SIMULACOES DE CASOS PARTICULARES"
	@echo "	Para simular um caso em particular:  		./p2 <lambda> <duracao media> <n amostras> <n canais> <tamanho fila>"

compile: p3.c
	@gcc p3.c -o p3 -lm

clean: 
	@rm -f p3
	@rm -f csv/*.csv

simulation_no_q: p3
	@./p2 200 0.008 1000000 1 0
	@echo ""
	@./p2 200 0.008 1000000 2 0
	@echo ""
	@./p2 200 0.008 1000000 3 0
	@echo ""
	@./p2 200 0.008 1000000 4 0
	@echo ""
	@./p2 200 0.008 1000000 5 0
	@echo ""

simulation_inf_q: p2
	@./p2 200 0.008 100000 1 -1
	@echo ""
	@./p2 200 0.008 100000 2 -1
	@echo ""
	@./p2 200 0.008 100000 3 -1
	@echo ""
	@./p2 200 0.008 100000 4 -1
	@echo ""
	@./p2 200 0.008 100000 5 -1
	@echo ""
