#ifndef GP_OPERATOR_H
#define GP_OPERATOR_H = 1

#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "queue.h"
#include "lista_ligada.h"
#include "operator.h"


static int GeneralPurposeOperator_initialize(size_t numberOfOperators, size_t queueSize);
static int GeneralPurposeOperator_eventArrival();
static int GeneralPurposeOperator_eventDeparture();

struct Operator GeneralPurposeOperator =
{	
	.initialize = &GeneralPurposeOperator_initialize, 
	.eventArrival = &GeneralPurposeOperator_eventArrival,
	.eventDeparture = &GeneralPurposeOperator_eventDeparture
};


static int GeneralPurposeOperator_initialize(size_t numberOfOperators, size_t queueSize)
{
	if(Operator_initialize(&GeneralPurposeOperator, numberOfOperators, queueSize) < 0)
		return -1;

	return 0;
}

static int GeneralPurposeOperator_eventArrival()
{
	// Gerar proximo evento de chegada
	lista* new_arrival_event = LinkedList.add(NULL, ARRIVAL, Queue.eventTime(GeneralPurposeOperator.channelQ));
	


	

}

 void GeneralPurposeOperator_eventDeparture_determineType(double currentTime)
{
	
	switch(rand_normalizado() > 0.3 ? DEPARTURE_TRANSFER : DEPARTURE_TERMINATE)
	{
		case DEPARTURE_TERMINATE:
			Queue.push(GeneralPurposeOperator.channelQ, LinkedList.add(NULL, DEPARTURE_TERMINATE, currentTime ));
		break;

		case DEPARTURE_TRANSFER:
			Queue.push(GeneralPurposeOperator.channelQ, LinkedList.add(NULL, DEPARTURE_TRANSFER, currentTime ));
		break;
	};

}

static int GeneralPurposeOperator_eventDeparture()
{
	
	size_t departureType = Queue.eventType(GeneralPurposeOperator.channelQ);
	double departureTime = Queue.eventTime(GeneralPurposeOperator.channelQ);

	Queue.pop(GeneralPurposeOperator.channelQ);

	switch(departureType)
	{
		case DEPARTURE_TERMINATE:
			
			if(!Queue.isEmpty(GeneralPurposeOperator.waitingQ))
			{	
				Queue.pop(GeneralPurposeOperator.waitingQ);
				GeneralPurposeOperator_eventDeparture_determineType(departureTime);
			}

			return 0;

		break;

		case DEPARTURE_TRANSFER:
			
			GeneralPurposeOperator.channelQ->waiting++;
			return 1;
		break;
	}
	
	return 0;
}

#endif