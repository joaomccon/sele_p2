#include <stdlib.h>

size_t total_packets;
size_t delayed_packets = 0;

static void statistics_lost_packet();
static size_t statistics_get_lost_packets();

static struct Statistics
{

    size_t packets_lost;

    void (*lostPacket)();
    size_t (*getLostPackets)();

}Statistics_struct;

struct Statistics Statistics = 
{
    .lostPacket = &statistics_lost_packet,
    .getLostPackets = &statistics_get_lost_packets,
    .packets_lost = 0
};

static void statistics_lost_packet()
{
    Statistics.packets_lost++;
}

static size_t statistics_get_lost_packets()
{
    return Statistics.packets_lost;
}

