#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "queue.h"
#include "lista_ligada.h"
#include "operator.h"
#include "general_purpose_operator.h"

extern struct Operator GeneralPurposeOperator;

static int AreaSpecificOperator_initialize(size_t numberOfOperators, size_t queueSize);
static int AreaSpecificOperator_eventArrival();
static int AreaSpecificOperator_eventDeparture();

struct Operator AreaSpecificOperator =
{	
	.initialize = &AreaSpecificOperator_initialize, 
	.eventArrival = &AreaSpecificOperator_eventArrival,
	.eventDeparture = &AreaSpecificOperator_eventDeparture,
	.waitingQ = NULL,
	.channelQ = NULL
};


static int AreaSpecificOperator_initialize(size_t numberOfOperators, size_t queueSize)
{
	if(Operator_initialize(&AreaSpecificOperator, numberOfOperators, queueSize) < 0)
		return -1;

	return 0;
}

static int AreaSpecificOperator_eventArrival()
{
	// Gerar proximo evento de chegada
	lista* new_arrival_event = LinkedList.add(NULL, ARRIVAL, Queue.eventTime(AreaSpecificOperator.channelQ));
	


	

}

static int AreaSpecificOperator_eventDeparture()
{
	
	size_t departureType = Queue.eventType(AreaSpecificOperator.channelQ);
	double departureTime = Queue.eventTime(AreaSpecificOperator.channelQ);
	
	Queue.pop(AreaSpecificOperator.channelQ);

	if(!Queue.isEmpty(AreaSpecificOperator.waitingQ))
	{	
		GeneralPurposeOperator.channelQ->waiting--;	// Permite que um dos eventos nos operadores de general purpose seja liberto
		Queue.pop(AreaSpecificOperator.waitingQ);
	}
	
	return 0;
}
