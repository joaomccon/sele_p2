#ifndef COMMON_H
#define COMMON_H = 1

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#define delta(lambda) (double)( (1.0/(double)lambda) * 0.2)
#define media(total, n) (double)(total/n)
#define rand_normalizado()(double) (rand()/(RAND_MAX + 1.0))

#define arrival_minute2second(x) (x/60)
#define arrival_hour2second(x) (x/(60*60))

#define poison_distribution (lambda) (-1.0/(double) lambda * log(rand_normalizado))
#define exponential_distribution ()()
#define gaussian_distribution ()()

#define INVALID_TYPE 0
#define ARRIVAL 2
#define DEPARTURE_TERMINATE 1
#define DEPARTURE_TRANSFER 3

#define ARRIVAL_FILL_INDICATOR 0
#define DEPARTURE_FILL_INDICATOR 1

double random_exp_time_step(double lambda)
{
	return -log(1- rand_normalizado()) / lambda;
}

double running_average(double current_sample, double current_average, size_t* number_of_samples)
{
	if(!number_of_samples)
		return -1;

	*number_of_samples++;

	return current_average * ((*number_of_samples - 1)/ *number_of_samples) + current_sample/(*number_of_samples);
}

#endif