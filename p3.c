#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "libs/lista_ligada.h"
#include "libs/common.h"
#include "libs/queue.h"
#include "libs/statistics.h"
#include "libs/histogram.h"
#include "libs/area_specific_operator.h"
#include "libs/general_purpose_operator.h"


int main(int argc, char** argv)
{

	size_t general_purpose_number, area_specific_number, general_purpose_queue_size, number_events;
	size_t current_event;

	if(argc != 5)
	{
		printf("p3 <number of general purpose operators> <number of area specific operators> <general purpose operators queue size> <number of clients requests>\n");
		return -1;
	}

	general_purpose_number = strtoul(argv[1], NULL, 10);
	area_specific_number = strtoul(argv[2], NULL, 10);
	general_purpose_queue_size = strtoul(argv[3], NULL, 10);
	number_events = strtoul(argv[4], NULL, 10);

	if(GeneralPurposeOperator.initialize(general_purpose_number, general_purpose_queue_size) || AreaSpecificOperator.initialize(area_specific_number, (size_t) -1))
		return -1;
	
	srand((unsigned)time(NULL));

	for(current_event = 0; current_event < number_events; current_event++)
	{

	}
	
	return 0;
}
